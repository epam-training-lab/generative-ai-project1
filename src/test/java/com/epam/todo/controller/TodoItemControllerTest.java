package com.epam.todo.controller;

import com.epam.todo.entity.TodoItem;
import com.epam.todo.service.TodoItemService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(TodoItemController.class)
class TodoItemControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TodoItemService todoItemService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void getAllTodoItems_ShouldReturnAllItems() throws Exception {
        TodoItem item1 = new TodoItem("Item 1", "Description 1");
        TodoItem item2 = new TodoItem("Item 2", "Description 2");

        given(todoItemService.findAll()).willReturn(Arrays.asList(item1, item2));

        mockMvc.perform(get("/api/todo"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].title").value("Item 1"))
                .andExpect(jsonPath("$[1].title").value("Item 2"));
    }

    @Test
    void getTodoItemById_WhenItemExists_ShouldReturnItem() throws Exception {
        TodoItem item = new TodoItem("Item 1", "Description 1");
        given(todoItemService.findById(anyLong())).willReturn(Optional.of(item));

        mockMvc.perform(get("/api/todo/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.title").value(item.getTitle()))
                .andExpect(jsonPath("$.description").value(item.getDescription()));
    }

    @Test
    void createTodoItem_ShouldReturnSavedItem() throws Exception {
        TodoItem item = new TodoItem("New Item", "New Description");
        given(todoItemService.save(any(TodoItem.class))).willReturn(item);

        mockMvc.perform(post("/api/todo")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(item)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title").value("New Item"))
                .andExpect(jsonPath("$.description").value("New Description"));
    }

}