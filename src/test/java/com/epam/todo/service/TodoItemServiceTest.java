package com.epam.todo.service;

import com.epam.todo.entity.TodoItem;
import com.epam.todo.repository.TodoItemRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TodoItemServiceTest {
    @Mock
    private TodoItemRepository todoItemRepository;

    @InjectMocks
    private TodoItemService todoItemService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void findAll_ShouldReturnAllItems() {
        when(todoItemRepository.findAll()).thenReturn(Arrays.asList(
                new TodoItem("Test Title 1", "Test Description 1"),
                new TodoItem("Test Title 2", "Test Description 2")));

        List<TodoItem> items = todoItemService.findAll();

        assertNotNull(items);
        assertEquals(2, items.size());
        verify(todoItemRepository, times(1)).findAll();
    }

    @Test
    void findById_ShouldReturnItem() {
        TodoItem item = new TodoItem("Test Title", "Test Description");
        item.setId(1L);
        when(todoItemRepository.findById(1L)).thenReturn(Optional.of(item));

        Optional<TodoItem> foundItem = todoItemService.findById(1L);

        assertTrue(foundItem.isPresent());
        assertEquals("Test Title", foundItem.get().getTitle());
        verify(todoItemRepository, times(1)).findById(1L);
    }

    @Test
    void save_ShouldSaveItem() {
        TodoItem item = new TodoItem("Test Title", "Test Description");
        when(todoItemRepository.save(any(TodoItem.class))).thenReturn(item);

        TodoItem savedItem = todoItemService.save(item);

        assertNotNull(savedItem);
        assertEquals("Test Title", savedItem.getTitle());
        verify(todoItemRepository, times(1)).save(any(TodoItem.class));
    }

    @Test
    void deleteById_ShouldDeleteItem() {
        Long itemId = 1L;
        doNothing().when(todoItemRepository).deleteById(itemId);

        todoItemService.deleteById(itemId);

        verify(todoItemRepository, times(1)).deleteById(itemId);
    }

}