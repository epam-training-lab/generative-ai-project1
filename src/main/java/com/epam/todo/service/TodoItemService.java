package com.epam.todo.service;

import com.epam.todo.entity.TodoItem;
import com.epam.todo.repository.TodoItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TodoItemService {

    private final TodoItemRepository todoItemRepository;

    @Autowired
    public TodoItemService(TodoItemRepository todoItemRepository) {
        this.todoItemRepository = todoItemRepository;
    }

    public List<TodoItem> findAll() {
        List<TodoItem> todoItems = new ArrayList<>();
        todoItemRepository.findAll().forEach(todoItems::add);
        return todoItems;
    }

    public Optional<TodoItem> findById(Long id) {
        return todoItemRepository.findById(id);
    }

    public TodoItem save(TodoItem todoItem) {
        return todoItemRepository.save(todoItem);
    }

    public void update(TodoItem todoItem) {
        todoItemRepository.save(todoItem);
    }

    public void deleteById(Long id) {
        todoItemRepository.deleteById(id);
    }

}