# Todo List Application

A simple RESTful API for managing a todo list, built with Spring Boot, Spring Data JPA, MySQL, and Gradle.

## Features

- CRUD operations for todo items
- Persistence in a MySQL database
- Simple exception handling for not found resources

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Java JDK 21 or later
- MySQL Server
- Gradle (optional, as you can use the Gradle wrapper included in the project)

## Setting Up the Database

1. Start your MySQL server.
2. Create a new database for the application:

```sql
CREATE DATABASE todo_db;
```

## Running the Application

To start the application run the following command:

```
./gradlew bootRun
```
